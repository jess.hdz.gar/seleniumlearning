package com.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Student {

	
	private int id;
	private String firstName;
	private String middleName;
	private String lastName;
	private Date birth;
	
	
	
	public Student(String firstName, String middleName, String lastName, Date birth) {
		super();
		this.id =(int) Math.random();
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.birth = birth;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getFullName() {
		return this.firstName.concat(" ").concat(this.middleName).concat(" ").concat(this.lastName);
	}

	public Date getBirth() {
		return birth;
	}
	public void birthFormat() {
		DateFormat df = new SimpleDateFormat("yyyy/dd/MM");
		System.out.println(df.format(birth));
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}
	
}
