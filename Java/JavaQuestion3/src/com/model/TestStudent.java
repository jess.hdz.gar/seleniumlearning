package com.model;

import java.util.Date;

public class TestStudent {

	public static void main(String[] args) {
		
		Student student1 = new Student("Juan", "Carlos", "Hernandez", new Date("12/03/1990"));
		Student student2 = new Student("Pedro", "Alejandro", "Gonzales", new Date("22/06/2005"));
		Student student3 = new Student("Maria", "Juana", "Lopez", new Date("1/09/2010"));
		
		System.out.println("Student1 information: ");
		System.out.println(student1.getFullName());
		student1.birthFormat();
		
		System.out.println("Student2 information: ");
		System.out.println(student2.getFullName());
		student2.birthFormat();
		
		System.out.println("Student3 information: ");
		System.out.println(student3.getFullName());
		student3.birthFormat();

	}

}
