package com.exercise.selenium;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Form {

	
	@Test
	public void testForm() {
		File file = new File("/Users/jess/Documentos/seleniumlearning/Selenium/SeleniumExercise/chromedriver");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());

		WebDriver driver = new ChromeDriver();
		driver.get("https://formy-project.herokuapp.com/form");

		submitForm(driver);

		WebElement alert = waitForAlert(driver);

		String alertText = alert.getText();
		assertEquals("The form was successfully submitted!", alertText);

		driver.quit();

	}

	private WebElement waitForAlert(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement alert = wait.until((ExpectedConditions.visibilityOfElementLocated(By.className("alert"))));
		return alert;
	}

	private void submitForm(WebDriver driver) {
		driver.findElement(By.id("first-name")).sendKeys("Jessika");
		driver.findElement(By.id("last-name")).sendKeys("Hernandez");
		driver.findElement(By.id("job-title")).sendKeys("Developer");
		driver.findElement(By.id("radio-button-2")).click();
		driver.findElement(By.id("checkbox-2")).click();
		driver.findElement(By.cssSelector("option[value='3']")).click();
		driver.findElement(By.id("datepicker")).sendKeys("01/01/2020");
		driver.findElement(By.id("datepicker")).sendKeys(Keys.RETURN);

		driver.findElement(By.cssSelector(".btn.btn-lg.btn-primary")).click();
	}
	
}
